# Arcanum Darkened
A game prototype made for Ludum Dare 40 Compo.

All assets made by me, within the 48 hour period of the contest.

Binaries at: https://pyrovx.itch.io/arcanum-darkened

## Description
An evil witch has awakened the Arcane Powers within you. Now she has locked you inside a room filled with innocent humans and is channeling Arcane Power into you, forcing you to use deadly spells inside the room and killing humans, turning you towards the dark side, or be consumed by the Power itself.

Try to hit as few humans as possible with your spells, while not letting your AP reach the maximum. You need to become proficient with the powers you have, so you can use them to break out of the shackles that hold you.