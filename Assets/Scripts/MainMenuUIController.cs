﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUIController : MonoBehaviour
{

    public GameObject MainMenu;
    public GameObject Guide;

    public void StartGame()
    {
        SceneManager.LoadScene("Main");
    }

    public void OpenGuide()
    {
        MainMenu.SetActive(false);
        Guide.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void CloseGuide()
    {
        MainMenu.SetActive(true);
        Guide.SetActive(false);
    }

}
