﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSpawner : MonoBehaviour
{

    public static NPCSpawner main;

    public GameObject NpcPrefab;
    public int InitialSpawnNumber;
    public Vector3 SpawnAreaMin;
    public Vector3 SpawnAreaMax;
    public float PlayerAreaRadius;
    public AnimationCurve spawnChance;
    public int maxCountOfNPC;
    public int minCountOfNPC;
    public int NPCCount;
    public int NPCSpawned;
    public bool spawning = true;

    private void Awake()
    {
        if (main != null)
        {
            Debug.Log("A NPCSpawner already exists?");
            Destroy(gameObject);
            return;
        }
        main = this;
    }

	// Use this for initialization
    private void Start ()
    {
        PlayerController.main.OnNPCKill += NPCKilled;
	    for (int i = 0; i < InitialSpawnNumber; i++)
	    {
	        Spawn();
	    }
        InvokeRepeating("RandomSpawn", 1.0f, 1.0f);
	}

    private void OnDestroy()
    {
        PlayerController.main.OnNPCKill -= NPCKilled;
    }

    private void RandomSpawn()
    {
        while (Random.value <= spawnChance.Evaluate((float)(Mathf.Clamp(NPCCount, minCountOfNPC, maxCountOfNPC)-minCountOfNPC) / (maxCountOfNPC-minCountOfNPC)))
        {
            Spawn();
        }
    }

    private void Spawn()
    {
        if (spawning)
        {
            NPCSpawned++;
            NPCCount++;
            Vector3 spawnPoint = Vector3.zero;
            while (spawnPoint.magnitude <= PlayerAreaRadius)
                spawnPoint = new Vector3(Random.Range(SpawnAreaMin.x, SpawnAreaMax.x), Random.Range(SpawnAreaMin.y, SpawnAreaMax.y), Random.Range(SpawnAreaMin.z, SpawnAreaMax.z));
            Quaternion spawnRotation = Quaternion.Euler(0.0f, Random.Range(0, 360), 0.0f);
            Instantiate(NpcPrefab, spawnPoint, spawnRotation);
        }
    }

    public void NPCKilled(int value)
    {
        NPCCount = NPCSpawned - value;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(Vector3.zero, PlayerAreaRadius);
    }
}
