﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineTarget : SpellTargeting
{
    public float DistanceMax = 10.0f;
    public float MaxUnstableOffset = 1.0f;
    public float UnstabilitySpeedAmplitude = 0.5f;

    private Vector3 targetDirection;
    private LineRenderer lineRenderer;
    private Vector3 position;
    private Vector3 offset;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    void Update()
    {
        Vector3 newOffset = offset + new Vector3(
                                Random.Range(-UnstabilitySpeedAmplitude * Time.fixedDeltaTime,
                                    UnstabilitySpeedAmplitude * Time.fixedDeltaTime), 0.0f,
                                Random.Range(-UnstabilitySpeedAmplitude * Time.fixedDeltaTime,
                                    UnstabilitySpeedAmplitude * Time.fixedDeltaTime));
        offset = Vector3.ClampMagnitude(newOffset, Mathf.Clamp(((float)PlayerController.main.AP / PlayerController.main.MaxAP - 0.6f), 0.0f, 0.4f) * 10.0f / 4.0f * MaxUnstableOffset);
        targetDirection = (position + offset).normalized;
        lineRenderer.SetPosition(1, (targetDirection * DistanceMax) + Vector3.up * 0.3f);
    }

    public override bool IsCastable()
    {
        return true;
    }

    public override void Move(Vector3 pos)
    {
        transform.position = pos + offset;
        this.position = pos;
        pos.y = 0.0f;
        targetDirection = (pos + offset).normalized;
        lineRenderer.SetPosition(1, (targetDirection * DistanceMax) + Vector3.up * 0.3f);

    }
}
