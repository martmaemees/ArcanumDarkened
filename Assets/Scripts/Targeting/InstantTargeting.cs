﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantTargeting : SpellTargeting
{
    public override bool IsCastable()
    {
        return true;
    }

    public override void Move(Vector3 pos)
    {
        // Intentionally empty
    }
}
