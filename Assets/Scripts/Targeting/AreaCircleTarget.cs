﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaCircleTarget : SpellTargeting
{

    public LayerMask CollisionMask;
    public Color DisabledColor;
    public float MaxUnstableOffset = 1.0f;
    public float UnstabilitySpeedAmplitude = 0.5f;

    public float Radius
    {
        get { return radius; }
        set
        {
            radius = value;
            GetComponent<SphereCollider>().radius = radius;
            particles.startSize = 3 * radius;
        }
    }

    private float radius;
    private ParticleSystem.MainModule particles;
    private Color particleColor;
    private Light pointLight;
    private Color lightColor;
    private int triggers = 0;
    private Rigidbody rb;
    private Vector3 position;
    private Vector3 offset;

    private void Awake()
    {
        particles = GetComponent<ParticleSystem>().main;
        particleColor = particles.startColor.color;
        GetComponent<SphereCollider>().radius = radius;
        particles.startSize = 3 * radius;
        rb = GetComponent<Rigidbody>();
        pointLight = GetComponentInChildren<Light>();
        lightColor = pointLight.color;
    }

    private void FixedUpdate()
    {
        Vector3 newOffset = offset + new Vector3(
                                Random.Range(-UnstabilitySpeedAmplitude * Time.fixedDeltaTime,
                                    UnstabilitySpeedAmplitude * Time.fixedDeltaTime), 0.0f,
                                Random.Range(-UnstabilitySpeedAmplitude * Time.fixedDeltaTime,
                                    UnstabilitySpeedAmplitude * Time.fixedDeltaTime));
        offset = Vector3.ClampMagnitude(newOffset, Mathf.Clamp(((float)PlayerController.main.AP / PlayerController.main.MaxAP - 0.6f), 0.0f, 0.4f) * 10.0f/4.0f * MaxUnstableOffset);
        rb.MovePosition(this.position + this.offset);
    }

    public override bool IsCastable()
    {
        return triggers == 0;
    }

    public override void Move(Vector3 position)
    {
        this.position = position;
        rb.MovePosition(this.position + this.offset);
    }

    private void OnTriggerEnter(Collider other)
    {
        // Layers didn't work.
        // Figured it out, keeping this shit anyways
        if (other.CompareTag("North Wall") || other.CompareTag("South Wall") || other.CompareTag("East Wall") || other.CompareTag("West Wall") || other.CompareTag("Shield"))
        {
            triggers++;
            SetColor(DisabledColor);
            pointLight.color = DisabledColor;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("North Wall") || other.CompareTag("South Wall") || other.CompareTag("East Wall") || other.CompareTag("West Wall") || other.CompareTag("Shield"))
        {
            triggers--;
            if (triggers == 0)
            {
                SetColor(particleColor);
                pointLight.color = lightColor;
            }
        }
    }

    private void SetColor(Color value)
    {
        var startColor = particles.startColor;
        startColor.color = value;
        particles.startColor = startColor;
    }
}
