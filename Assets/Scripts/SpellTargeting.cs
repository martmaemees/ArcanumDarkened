﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// ReSharper disable CompareOfFloatsByEqualityOperator

public abstract class SpellTargeting : MonoBehaviour
{
    public abstract bool IsCastable();
    public abstract void Move(Vector3 pos);

}
