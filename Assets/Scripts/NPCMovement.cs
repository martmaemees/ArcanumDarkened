﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCMovement : MonoBehaviour {

    public float Speed;

    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        rb.MovePosition(transform.position + transform.forward * Speed * Time.fixedDeltaTime);
	}

    private void OnTriggerEnter(Collider other)
    {
        if (rb == null)
            rb = GetComponent<Rigidbody>();
        if(other.CompareTag("North Wall"))
        {
            rb.MoveRotation(Quaternion.LookRotation(Vector3.Reflect(transform.forward, Vector3.back * transform.position.y), new Vector3(0.0f, 1.0f, 0.0f)));
        }
        else if(other.CompareTag("South Wall"))
        {
            rb.MoveRotation(Quaternion.LookRotation(Vector3.Reflect(transform.forward, Vector3.forward * transform.position.y), new Vector3(0.0f, 1.0f, 0.0f)));
        }
        else if(other.CompareTag("East Wall"))
        {
            rb.MoveRotation(Quaternion.LookRotation(Vector3.Reflect(transform.forward, Vector3.right * transform.position.y), new Vector3(0.0f, 1.0f, 0.0f)));
        }
        else if(other.CompareTag("West Wall"))
        {
            rb.MoveRotation(Quaternion.LookRotation(Vector3.Reflect(transform.forward, Vector3.right * transform.position.y), new Vector3(0.0f, 1.0f, 0.0f)));
        }
        else if (other.CompareTag("Shield"))
        {
            Vector3 closestPoint = other.ClosestPoint(rb.position);
            closestPoint.y = rb.position.y;
            rb.MoveRotation(Quaternion.LookRotation(Vector3.Reflect(transform.forward,
                closestPoint - new Vector3(0.0f, closestPoint.y, 0.0f)), new Vector3(0.0f, 1.0f, 0.0f)));
        }
    }
}
