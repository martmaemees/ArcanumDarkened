﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spell : MonoBehaviour
{

    public bool CheckMouseMovementOptimization = false;
    public abstract int XPGain();
    public abstract int APCost();

    [Space(8)]
    public GameObject TargetingObject = null;
    public SpellTargeting targetingScript = null;
    public LayerMask GroundLayer;

    private bool targeting = true;
    private bool canCast = true;

    // Use this for initialization
    void Start ()
	{
	    TargetingObject = transform.GetChild(0).gameObject;
	    targetingScript = TargetingObject.GetComponent<SpellTargeting>();
	    Init();
	}
	
	// Update is called once per frame
	void Update () {
	    if (targeting)
	    {
	        canCast = targetingScript.IsCastable() && PlayerController.main.AP >= APCost();
	        if (canCast && Input.GetMouseButtonDown(0))
	        {
	            PlayerController.main.CastSpell(APCost(), XPGain());
	            targeting = false;
                SetTarget(TargetingObject.transform.position);
	            TargetingObject.SetActive(false);
                if(transform.childCount > 1)
                {
                    transform.GetChild(1).position = TargetingObject.transform.position;
                    transform.GetChild(1).gameObject.SetActive(true);
                }
            }
	    }
	    else
	    {
	        Move();
	        if (FinishedMove())
	        {
	            PlayerController.main.NPCsKilled(Hit());
                Destroy(gameObject);
	        }
	    }
	}

    void FixedUpdate()
    {
        if (targeting)
        {
            if (!CheckMouseMovementOptimization || Math.Abs(Input.GetAxisRaw("Mouse X")) > 0.002f || Math.Abs(Input.GetAxisRaw("Mouse Y")) > 0.002f)
            {
                RaycastHit hit;
                if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, float.MaxValue,
                    GroundLayer.value))
                {
                    Vector3 pos = hit.point;
                    pos.y = 0.3f;
                    targetingScript.Move(pos);
                }
            }
        }
    }

    public abstract void Init();
    public abstract void SetTarget(Vector3 pos);
    public abstract void Move();
    public abstract bool FinishedMove();
    public abstract int Hit();
}
