﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController main;

    [Header("GUI")]
    public Text KilledCounter;
    public Text LevelCounter;
    public Text APText;
    public Text XPText;
    public Slider XPSlider;
    public Slider APSlider;
    [Header("Buttons")]
    public Button FirebombButton;
    public Button FlamewallButton;
    public Button GaspuffButton;
    public Button BreakChainsButton;
    [Header("Pause Menu")]
    public GameObject PauseMenuPanel;
    [Header("GameOver")]
    public Image DarkPanel;
    public GameObject OverflowText;
    public GameObject DarknessText;
    public GameObject BreakFreeText;
    public Text GOHumanKillsText;
    public Text GOLevelText;
    public Text GOXPText;
    public CanvasGroup GOCanvasGroup;
    public Button GOMainMenuButton;

    private bool gameOver = false;

    void Awake()
    {
        if (main != null)
        {
            Debug.Log("UIController already exists?");
            Destroy(gameObject);
            return;
        }
        main = this;

        PlayerController.main.OnNPCKill += KillUpdate;
        PlayerController.main.OnXPChange += XPUpdate;
        PlayerController.main.OnAPChange += APUpdate;
        PlayerController.main.OnMaxXPChange += MaxXPUpdate;
        PlayerController.main.OnMaxAPChange += MaxAPUpdate;
        PlayerController.main.OnLevelChange += LevelUpdate;
    }

    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if(Time.timeScale == 1.0f)
                PauseGame();
            else
                UnpauseGame();
        }
    }

    void OnDestroy()
    {
        PlayerController.main.OnNPCKill -= KillUpdate;
        PlayerController.main.OnXPChange -= XPUpdate;
        PlayerController.main.OnAPChange -= APUpdate;
        PlayerController.main.OnMaxXPChange -= MaxXPUpdate;
        PlayerController.main.OnMaxAPChange -= MaxAPUpdate;
        PlayerController.main.OnLevelChange -= LevelUpdate;
    }

    public void SelectSpellButton(int index)
    {
        PlayerController.main.SelectSpell(PlayerController.main.Spells[index]);
    }

    public void KillUpdate(int count)
    {
        KilledCounter.text = count.ToString();
        GOHumanKillsText.text = "Humans Killed: " + count;
    }

    public void XPUpdate(int value)
    {
        XPSlider.value = value;
        XPText.text = value + "/" + XPText.text.Split('/')[1];
        GOXPText.text = "XP: " + value;
    }

    public void APUpdate(int value)
    {
        APSlider.value = value;
        APText.text = value + "/" + APText.text.Split('/')[1];

        FirebombButton.interactable = value >= 15;
        FlamewallButton.interactable = value >= 40;
        GaspuffButton.interactable = value >= 100;
        BreakChainsButton.interactable = value >= 0;
    }

    public void MaxXPUpdate(int value)
    {
        XPSlider.maxValue = value;
        XPText.text = XPText.text.Split('/')[0] + "/" + value;
    }

    public void MaxAPUpdate(int value)
    {
        APSlider.maxValue = value;
        APText.text = APText.text.Split('/')[0] + "/" + value;
    }

    public void LevelUpdate(int value)
    {
        LevelCounter.text = "Level: " + value;
        GOLevelText.text = "Level: " + value;

        switch (value)
        {
            case 3:
                FlamewallButton.gameObject.SetActive(true);
                break;
            case 5:
                GaspuffButton.gameObject.SetActive(true);
                break;
            case 7:
                BreakChainsButton.gameObject.SetActive(true);
                break;
        }
    }

    public void GameOver(GameOverType type)
    {
        gameOver = true;
        switch (type)
        {
            case GameOverType.Darkness:
                DarknessText.gameObject.SetActive(true);
                OverflowText.gameObject.SetActive(false);
                BreakFreeText.gameObject.SetActive(false);
                break;
            case GameOverType.Overflow:
                DarknessText.gameObject.SetActive(false);
                OverflowText.gameObject.SetActive(true);
                BreakFreeText.gameObject.SetActive(false);
                break;
            case GameOverType.BreakFree:
                BreakFreeText.gameObject.SetActive(true);
                DarknessText.gameObject.SetActive(false);
                OverflowText.gameObject.SetActive(false);
                break;
        }
        StartCoroutine(GameOverFadeIn());
    }

    IEnumerator GameOverFadeIn()
    {
        while (DarkPanel.color.a < 1)
        {
            Color c = DarkPanel.color;
            c.a += Time.deltaTime * 0.4f;
            DarkPanel.color = c;
            yield return new WaitForEndOfFrame();
        }
        while (GOCanvasGroup.alpha < 1)
        {
            GOCanvasGroup.alpha += Time.deltaTime * 0.4f;
            yield return new WaitForEndOfFrame();
        }
        GOMainMenuButton.interactable = true;
    }

    public void MainMenu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Main Menu");
    }

    public void PauseGame()
    {
        if (!gameOver)
        {
            Time.timeScale = 0.0f;
            PauseMenuPanel.SetActive(true);
        }
    }

    public void UnpauseGame()
    {
        if (!gameOver)
        {
            Time.timeScale = 1.0f;
            PauseMenuPanel.SetActive(false);
        }
    }
}
