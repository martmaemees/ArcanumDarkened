﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public struct SpellSet
{
    public String CastKey;
    public GameObject Spell;
    public int APCost;
    public int MinLevel;
}

[Serializable]
public struct LevelStats
{
    public int MaxXP;
    public int MaxAP;
    public int APGain;
}
public enum GameOverType { Overflow, Darkness, BreakFree }

public class PlayerController : MonoBehaviour
{   
    public static PlayerController main;

    public delegate void IntChangeAction(int count);

    public event IntChangeAction OnNPCKill;
    public event IntChangeAction OnAPChange;
    public event IntChangeAction OnXPChange;
    public event IntChangeAction OnMaxAPChange;
    public event IntChangeAction OnMaxXPChange;
    public event IntChangeAction OnLevelChange;
    public event IntChangeAction OnAPRegenChange;

    public Gradient ColorGradient;
    public int MaxHumanKills = 100;
    public ParticleSystem FinalExplosionParticles;
    public ParticleSystem DarkFinalExplosionParticles;
    public ParticleSystem FreeExplosion;
    public Material ShieldMaterial;
    [Header("Spell Data")]
    public List<SpellSet> Spells;
    [Header("Level Data")]
    public List<LevelStats> LevelStats;

    public int XP
    {
        get { return xp; } 
        set
        {
            xp = value;
            if (OnXPChange != null) OnXPChange(xp);
            if (xp >= maxXP)
                LevelUp();
        }
    }
    public int AP
    {
        get { return ap; }
        set
        {
            ap = value;
            if (OnAPChange != null) OnAPChange(ap);
            if (ap >= MaxAP)
                GameOver(GameOverType.Overflow);
        }
    }
    public int MaxXP
    {
        get { return maxXP; }
        set
        {
            maxXP = value;
            if (OnMaxXPChange != null) OnMaxXPChange(maxXP);
        }
    }
    public int MaxAP
    {
        get { return maxAP; }
        set
        {
            maxAP = value;
            if (OnMaxAPChange != null) OnMaxAPChange(maxAP);
        }
    }
    public int APRegen
    {
        get { return apRegen; }
        set
        {
            apRegen = value;
            if (OnAPRegenChange != null) OnAPRegenChange(apRegen);
        }
    }
    public int Level
    {
        get { return level; }
        set
        {
            level = value;
            if (OnLevelChange != null) OnLevelChange(level);
        }
    }

    private int npcsKilled = 0;
    private GameObject castingSpell;
    private String castingSpellKey = "";
    private int xp = 0;
    private int ap = 0;
    private int maxAP = 100;
    private int maxXP = 100;
    private int apRegen = 4; // Per second
    private float apTimer = 0.0f;
    private int level = 0;
    private Material playerMaterial;
    private bool running = true;
    private AudioSource evilLaughSource;

	// Use this for initialization
	void Awake () {
	    if (main != null)
	    {
            Debug.Log("Already have a main player set!");
	        Destroy(gameObject);
	        return;
	    }
	    main = this;
	    playerMaterial = GetComponent<MeshRenderer>().material;
	    ShieldMaterial = GameObject.Find("Player Shield").GetComponent<MeshRenderer>().material;
	    evilLaughSource = GetComponent<AudioSource>();
	}

    void Start() { LevelUp(); }
	
	// Update is called once per frame
	void Update ()
	{
	    if (running)
	    {
	        apTimer += Time.deltaTime;
	        if (apTimer >= 1.0f)
	        {
	            apTimer -= 1.0f;
	            AP += APRegen;
	        }

	        foreach (SpellSet set in Spells)
	        {
	            if (AP >= set.APCost && Input.GetButtonDown(set.CastKey))
	            {
	                SelectSpell(set);
	            }
	        }
        }
	}

    void FixedUpdate()
    {
        if (running)
        {
            Vector3 pos = transform.position;
            pos.y = (Mathf.Sin(Time.time * 5.0f) + 1) / 5.0f + 1.01f;
            transform.position = pos;
        }
    }

    public void CastSpell(int apCost, int xpGain)
    {
        if (running)
        {
            castingSpell = null;
            castingSpellKey = "";
            AP -= apCost;
            XP += xpGain;
        }
    }

    public void SelectSpell(SpellSet set)
    {
        if (running)
        {
            if (set.MinLevel <= Level && set.CastKey != castingSpellKey && AP >= set.APCost)
            {
                if (castingSpell != null)
                    Destroy(castingSpell);
                castingSpell = (GameObject)Instantiate(set.Spell, Vector3.up * 0.5f, Quaternion.identity);
                castingSpellKey = set.CastKey;
            }
        }
    }

    public void NPCsKilled(int count)
    {
        if (count > 3 && UnityEngine.Random.Range(0, 100) <= 10)
        {
            evilLaughSource.Play();
        }
        npcsKilled += count;
        if (OnNPCKill != null) OnNPCKill(npcsKilled);
        if(npcsKilled > MaxHumanKills)
            GameOver(GameOverType.Darkness);

        Color color = ColorGradient.Evaluate((float) npcsKilled / MaxHumanKills);
        playerMaterial.color = color;
        color.a = ShieldMaterial.color.a;
        ShieldMaterial.color = color;
    }

    public void LevelUp()
    {
        Level++;
        XP = 0;
        MaxXP = LevelStats[Level - 1].MaxXP;
        MaxAP = LevelStats[Level - 1].MaxAP;
        APRegen = LevelStats[Level - 1].APGain;
    }

    public void GameOver(GameOverType type)
    {
        if (running)
        {
            running = false;
            NPCSpawner.main.spawning = false;
            if (type != GameOverType.BreakFree)
            {
                Collider[] colliders = Physics.OverlapBox(Vector3.zero, new Vector3(40.0f, 40.0f, 40.0f));
                foreach (Collider c in colliders)
                {
                    if (c.CompareTag("NPC"))
                    {
                        Destroy(c.gameObject);
                        NPCsKilled(1);
                    }
                }
            }
            if (type == GameOverType.Overflow)
            {
                FinalExplosionParticles.Play();
            }
            else if (type == GameOverType.Darkness)
            {
                evilLaughSource.Play();
                DarkFinalExplosionParticles.Play();
            }
            else if (type == GameOverType.BreakFree)
            {
                FreeExplosion.Play();
            }

            UIController.main.GameOver(type);
        }
    }

}


