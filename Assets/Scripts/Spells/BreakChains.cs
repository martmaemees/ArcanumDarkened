﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngineInternal;

public class BreakChains : Spell
{

    public override int APCost()
    {
        return PlayerController.main.AP;
    }

    public override bool FinishedMove()
    {
        return true;
    }

    public override int Hit()
    {
        PlayerController.main.GameOver(GameOverType.BreakFree);
        return 0;
    }

    public override void Init()
    {
        Hit();
    }

    public override void Move()
    {
        
    }

    public override void SetTarget(Vector3 pos)
    {
        
    }

    public override int XPGain()
    {
        return 0;
    }
}
