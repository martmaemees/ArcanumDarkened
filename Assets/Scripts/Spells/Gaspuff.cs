﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gaspuff : Spell
{
    public float Radius = 4.0f;
    public GameObject GasPrefab;

    private Vector3 targetPos;

    public override int APCost()
    {
        return 100;
    }

    public override bool FinishedMove()
    {
        return true;
    }

    public override int Hit()
    {
        Instantiate(GasPrefab, targetPos, Quaternion.identity);
        return 0;
    }

    public override void Init()
    {
        ((AreaCircleTarget)targetingScript).Radius = Radius;
    }

    public override void Move()
    {
        
    }

    public override void SetTarget(Vector3 pos)
    {
        targetPos = pos;
    }

    public override int XPGain()
    {
        return 100;
    }
}
