﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Firebomb : Spell
{ 
    public float Radius = 3.0f;
    public float TriggerTime = 1.0f;
    public GameObject ExplosionPrefab;
    public LayerMask NpcLayerMask;

    private float triggerTimer = 0.0f;
    private Vector3 targetPos;

    public override bool FinishedMove()
    {
        return triggerTimer >= TriggerTime;
    }

    public override void SetTarget(Vector3 pos)
    {
        targetPos = pos;
    }

    public override int Hit()
    {
        Instantiate(ExplosionPrefab, targetPos, Quaternion.identity);
        Collider[] killedColliders = Physics.OverlapSphere(targetPos, Radius, NpcLayerMask);
        foreach (Collider c in killedColliders)
        {
            Destroy(c.gameObject);
        }
        return killedColliders.Length;
    }

    public override void Init()
    {
        ((AreaCircleTarget)targetingScript).Radius = Radius;
    }

    public override void Move()
    {
        triggerTimer += Time.deltaTime;
    }

    public override int XPGain()
    {
        return 10;
    }

    public override int APCost()
    {
        return 15;
    }
}
