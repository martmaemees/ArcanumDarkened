﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gas : MonoBehaviour
{

    public LayerMask NPCMask;

    private void Start()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 4, NPCMask.value);
        PlayerController.main.NPCsKilled(colliders.Length);
        foreach (Collider c in colliders)
        {
            Destroy(c.gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if ((int)Mathf.Pow(2, other.gameObject.layer) == NPCMask.value)
        {
            Destroy(other.gameObject);
            PlayerController.main.NPCsKilled(1);
        }
    }

}
