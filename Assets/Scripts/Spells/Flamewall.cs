﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flamewall : Spell
{
    public GameObject ProjectilePrefab;

    private Vector3 targetDirection;

    public override int APCost()
    {
        return 40;
    }

    public override bool FinishedMove()
    {
        return true;
    }

    public override int Hit()
    {
        Instantiate(ProjectilePrefab, Vector3.zero, Quaternion.LookRotation(targetDirection, Vector3.up));
        return 0;
    }

    public override void Init()
    {
        
    }

    public override void Move()
    {
        
    }

    public override void SetTarget(Vector3 pos)
    {
        pos.y = 0.0f;
        targetDirection = pos.normalized;
    }

    public override int XPGain()
    {
        return 40;
    }
}
