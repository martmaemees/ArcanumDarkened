﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlamewallProjectile : MonoBehaviour
{

    public float ProjectileSpeed = 5.0f;
    public float ProjectileDistance = 10.0f;

    private Rigidbody rb;
    private float distanceCounter = 0.0f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rb.MovePosition(transform.position + transform.forward * ProjectileSpeed * Time.fixedDeltaTime);
        distanceCounter += ProjectileSpeed * Time.fixedDeltaTime;
        if(distanceCounter >= ProjectileDistance)
            Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("NPC"))
        {
            PlayerController.main.NPCsKilled(1);
            Destroy(other.gameObject);
        }
    }

}
