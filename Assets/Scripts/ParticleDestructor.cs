﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestructor : MonoBehaviour
{

    public ParticleSystem Particles;

	// Use this for initialization
	void Start ()
	{
	    Particles = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Particles.isStopped)
            Destroy(gameObject);
	}
}
