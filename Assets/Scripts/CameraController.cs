﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float MoveSpeed = 40.0f;
    public float RotationSpeed = 20.0f;
    public Vector3 CameraBounds = new Vector3(30.0f, 30.0f, 30.0f);

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//Vector3 inputVector = new Vector3(transform.forward.x * Input.GetAxis("Horizontal"), 0.0f, transform.forward.z * Input.GetAxis("Vertical"));
	    Vector3 inputVector =
	        Vector3.ProjectOnPlane(transform.forward, Vector3.down).normalized * Input.GetAxis("Vertical") + transform.right * Input.GetAxis("Horizontal");
	    float rotationValue = Input.GetAxis("CameraRotation");

	    Vector3 newPosition = transform.position + inputVector * MoveSpeed * Time.deltaTime;
	    if (newPosition.x > CameraBounds.x)
	        newPosition.x = CameraBounds.x;
        else if (newPosition.x < -CameraBounds.x)
	        newPosition.x = -CameraBounds.x;
	    if (newPosition.z > CameraBounds.z)
	        newPosition.z = CameraBounds.z;
        else if (newPosition.z < -CameraBounds.z)
	        newPosition.z = -CameraBounds.z;

        transform.SetPositionAndRotation(newPosition, transform.rotation);
	    transform.Rotate(Vector3.up, rotationValue * RotationSpeed * Time.deltaTime, Space.World);

        
    }
}
